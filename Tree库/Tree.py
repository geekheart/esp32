import os

class Tree():
    def __init__(self,path=os.getcwd()):
        print('.')
        self.cache=1
        self.dic=0
        self.file=0
        self.tree(path)

    def isdir(self,name):
        for i in os.ilistdir():
            if i[0] == name and i[1] == 16384:
                return True
        return False
        
    def __tree(self,path):
        for i in os.listdir(path):
            for j in range(self.cache):
                print('|',end='')
            if self.isdir(i):
                print('-- '+i)
                self.cache += 1
                self.dic += 1
                self.__tree(path+'\\'+i)
                self.cache -= 1
            else:
                print('-- '+i)
                self.file += 1  
                
    def tree(self,path=''):
        if path == '':
            path = os.getcwd()
        self.__tree(path)
        print("%d directories, %d files"%(self.dic, self.file)) 
        self.dic=0
        self.file=0

    def cp(self, filepath1, filepath2=''):
        if filepath1 == filepath2 or filepath2 == '':
            for num,contant in enumerate(filepath1):
                if contant == '.':
                    filepath2 = filepath1[:num]+'_cp'+filepath1[num:]
        with open(filepath1, 'rb') as f1:
            with open(filepath2, 'wb') as f2:
                f2.write(f1.read())
        print('%s --> %s copy succssful'%(filepath1,filepath2))

    def rm(self,filename,mode='f'):
        if mode == 'f':
            os.remove(filename)
            print('file %s remove succssful'%(filename))
        if mode == 'd':
            os.rmdir(filename)
            print('dir %s remove succssful'%(filename))
        if mode == 'a':
            if self.isdir(filename):
                try:
                    self.rm(filename,'d')
                except:
                    for i in os.listdir(filename):
                        self.cd(filename)
                        self.rm(i,'a')
                        self.cd('../')
                    self.rm(filename,'d')
            else:
                self.rm(filename)
                
    def pwd(self):
        return os.getcwd()

    def cd(self,filepath):
        if filepath == '../':
            pathlist = self.pwd().split('/')
            pathlist = pathlist[:-1]
            path = ''
            for i in pathlist:
                path = path+'/'+i
            os.chdir(path)                
        else:
            os.chdir(filepath)

    def mv(self,filename1,filename2):
        if filename1 != filename2:
            self.cp(filename1,filename2)
            self.rm(filename1)

    def mkdir(self,dirpath):
        os.mkdir(dirpath)
              