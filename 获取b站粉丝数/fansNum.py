import KeyScan
import urequests
import LinkNet
import oled


def Get_info(name):
    if name == "fans":
        r = urequests.get("https://api.bilibili.com/x/relation/stat?vmid=442752399&jsonp=jsonp")
        return r.json()['data']['follower']
    if name == "praise":
        r = urequests.get("https://api.bilibili.com/x/space/upstat?mid=442752399&jsonp=jsonp")
        return r.json()['data']['likes']
    if name == "view":
        r = urequests.get("https://api.bilibili.com/x/space/upstat?mid=442752399&jsonp=jsonp")
        return r.json()['data']["archive"]['view']

def main():
    o = oled.OLED()
    LinkNet.linkNet()
    for i in range(4):
        o.Chinese(0,32+16*i,i)
    o.String(2,0,"fans:")
    o.String(2,40,str(Get_info("fans")))
    o.String(4,0,"praise:")
    o.String(4,56,str(Get_info("praise")))
    o.String(6,0,"view:")
    o.String(6,40,str(Get_info("view")))