# esp32 TEST

## 介绍

这是我自己建立的一个和esp32的基于micropython固件的库，自己写的，有什么问题可以给我留言

## 软件架构

**多线程**：这个库实际上并不完善，在micropython里也仅仅是处在开发阶段，这里参照python3的threading模块给它封装了一下但是相比起来还是阉割了很多，亲测两个线程无压力但是如果太多就不行了

**oled屏幕**：这个模块基于模拟i2c通讯实现的，实际上esp32并不支持硬件i2c。对于字库，这里制作了8x160准ascii的英文显示和8x6极限ascii显示，还有自己的中文字库，中文字库采取纵向取模下高位,数据排列:从左到右从上到下

**网络连接**：网络连接属于基础的库，这里基本上借鉴了官方的写法

**mqtt网络通讯**：这里对于micropython的版本有要求尽量选取最新版的micropython固件版本

**usart串口通讯**：基本上是官方给的例子，由于很重要被我纳入进来

**矩阵键盘**：这里是针对自己的2*3的矩阵键盘制作的驱动大家可以借鉴然后写出自己的驱动，有些端口没有输入功能

**获取b站粉丝**：该库是基于oled库和urequests库做的，api是我分析抓包结果得到的

**模拟SPI TFT屏幕**：这里是lcd彩屏的驱动，一开始移植了stm32的驱动但是esp32有硬件spi驱动和打包好的软件spi驱动接口，有空我再试着改进这个驱动和写出硬件SPI驱动。如果是正版的esp32应该主频只会跑一半，所以我在刷屏这一块开启了最高频率来增加刷屏速度

**Tree库**： 我看micropython里没有文件管理的库，于是自己写了一个

**连接Ali云**: 疫情期间在家摸索的写出的，连上的阿里云IoT自己也通过IoT MQTT Panel这个app完成了手机端的操控，然后就传上来了，总体来说程序编的有点粗糙，大家作为参考吧，关于这方面我可能会发布视频去讲解这一整套的开发流程

## 使用示例

### 多线程

```python
>>> import thread_example
>>> thread_example.main()
```

**运行结果：**

![运行结果](https://gitee.com/geekheart/esp32/raw/master/img-folder/threading.png)

### oled屏幕

```python
>>> import oled
>>> o = oled.OLED() # 构建oled对象
>>> o.Char(0,0,"A") # 在坐标0，0处显示A
>>> o.Char(0,8,"a") # 在坐标0，8处显示a
>>> o.MiniChar(0,16,"A") # 在坐标0，16处显示A
>>> o.MiniChar(0,22,"a") # 在坐标0，22处显示a
>>> o.String(2,0,'GeekHeart') # 在坐标2，0处显示GeekHeart
>>> o.Chinese(4,0,2) # 在坐标4，0处显示第三个汉字（需要自行取模）
>>> o.Chinese(4,16,3) # 在坐标4，16处显示四个汉字（需要自行取模）
```

**运行结果：**

![运行结果](https://gitee.com/geekheart/esp32/raw/master/img-folder/oled.jpg)

### 网络连接

```python
>>> import LinkNet
>>> LinkNet.GetAPName() # 获取扫描到的Wifi
>>> LinkNet.LinkNet(name='你的wifi名',passwd='你的wifi密码',timeout=3000) # timeout单位毫秒
```

**运行结果：**

![运行结果](https://gitee.com/geekheart/esp32/raw/master/img-folder/LinkNet.png)

### mqtt网络通讯

```python
>>> import example_sub_led
>>> example_sub_led.main() # 需要用mosquitto搭建好mqtt服务器，提前修改好SERVER，TOPIC等参数
```

### usart串口通讯

```python
>>> import uart # 官方给的库主要重在参考价值
```

### 矩阵键盘

```python
>>> import KeyScan
>>> KeyScan.init() # 引脚初始化
>>> Key.Scan.scan() # 返回值为按钮序号
```

### 获取b站粉丝

```python
>>> import fansNum
>>> fansNum.main() # 这个麦叔编程不是我的，我的号粉丝太少就爬取别人的做个示范
```

**运行结果：**

![运行结果](https://gitee.com/geekheart/esp32/raw/master/img-folder/fansNum.jpg)

### 模拟SPI TFT屏幕

```python
>>> import TFT_LCD
>>> lcd = TFT_LCD.LCD()
>>> lcd.Clear(TFT_LCD.WHITE) # 由于疫情LCD不在身边，此为年前移植的，未移植GUI库
```

### Tree库

```python
>>> import Tree
>>> t = Tree.Tree() # 创建对象的时候会以树状图的形式展示文件和文件夹
>>> t.cp('/Blinker/test/1.py','/Blinker/test2/1.py')
# 拷贝文件；只有一个参数或者两个参数相同会在原地复制一个名字带有_cp的文件
>>> t.rm(filename,mode='f')
# 删除文件；这里的第二个参数是要删除的模式有‘f’文件，‘d’文件夹，‘a’所有，缺省值为‘f’
>>> t.mv(file1,file2)
# 移动文件；如果file1和file2的路径相同该方法运行结果就是重命名
>>> t.mkdir(dirname) # 创建文件夹
>>> t.tree(path=os.path) # 展示树path默认为当前路径
>>> t.pwd() # 展示当前所处路径
>>> t.isdir(filename) # 判断是否是文件夹
```

**运行结果：**
![运行结果](https://gitee.com/geekheart/esp32/raw/master/img-folder/Tree.png)

### 连接阿里云

```python
>>> import AliIot
>>> AliIot.main()
```

**运行结果：**
![运行结果](https://gitee.com/geekheart/esp32/raw/master/img-folder/AliIoT.png)
