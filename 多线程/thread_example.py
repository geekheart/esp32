import threading,time


def hello1():
    for i in range(5):
        time.sleep_ms(1000)
        print("time1:%d"%(i))


def hello2():
    for i in range(5):
        time.sleep_ms(1000)
        print("time2:%d"%(i))

def main():
    Thd1 = threading.Thread(target=hello1)
    Thd2 = threading.Thread(target=hello2)
    Thd1.start()
    Thd2.start()
