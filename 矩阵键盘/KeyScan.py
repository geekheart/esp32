import machine
pin14 = 0
pin27 = 0
pin25 = 0
pin26 = 0
pin33 = 0

def init():
    global pin14
    global pin27
    global pin25
    global pin26
    global pin33
    pin14 = machine.Pin(14, machine.Pin.OUT)
    pin27 = machine.Pin(27, machine.Pin.OUT) 
    pin25 = machine.Pin(25, machine.Pin.IN, machine.Pin.PULL_UP)
    pin26 = machine.Pin(26, machine.Pin.IN, machine.Pin.PULL_UP)
    pin33 = machine.Pin(33, machine.Pin.IN, machine.Pin.PULL_UP)
    
def scan():
    pin14.value(0)
    pin27.value(1)
    if pin25.value()==0: 
        return 1
    elif pin26.value()==0:
        return 2
    elif pin33.value()==0:
        return 3
    pin14.value(1)
    pin27.value(0)
    if pin25.value()==0: 
        return 4
    elif pin26.value()==0:
        return 5
    elif pin33.value()==0:
        return 6
    
