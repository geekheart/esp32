import TFT_SPI
import time
import machine

X_MAX_PIXEL = 128
Y_MAX_PIXEL = 160

RED = 0xf800
GREEN = 0x07e0
BLUE  = 0x001f
WHITE = 0xffff
BLACK = 0x0000
YELLOW = 0xFFE0
GRAY0 = 0xEF7D   
GRAY1 = 0x8410         
GRAY2 = 0x4208 

class LCD(object):
    def __init__(self):
        self.SPI = TFT_SPI.SPI_BUS()
        self.SPI.Lcd_Reset()
    
        self.SPI.Lcd_WriteIndex(0x11)
        time.sleep(0.12)
            
        self.SPI.Lcd_WriteIndex(0xB1)
        self.SPI.Lcd_WriteData(0x01) 
        self.SPI.Lcd_WriteData(0x2C) 
        self.SPI.Lcd_WriteData(0x2D) 
    
        self.SPI.Lcd_WriteIndex(0xB2) 
        self.SPI.Lcd_WriteData(0x01) 
        self.SPI.Lcd_WriteData(0x2C) 
        self.SPI.Lcd_WriteData(0x2D) 
    
        self.SPI.Lcd_WriteIndex(0xB3)
        self.SPI.Lcd_WriteData(0x01) 
        self.SPI.Lcd_WriteData(0x2C) 
        self.SPI.Lcd_WriteData(0x2D) 
        self.SPI.Lcd_WriteData(0x01) 
        self.SPI.Lcd_WriteData(0x2C) 
        self.SPI.Lcd_WriteData(0x2D)
        
        self.SPI.Lcd_WriteIndex(0xB4)
        self.SPI.Lcd_WriteData(0x07)
        
       
        self.SPI.Lcd_WriteIndex(0xC0) 
        self.SPI.Lcd_WriteData(0xA2) 
        self.SPI.Lcd_WriteData(0x02) 
        self.SPI.Lcd_WriteData(0x84) 
        self.SPI.Lcd_WriteIndex(0xC1)
        self.SPI.Lcd_WriteData(0xC5) 
    
        self.SPI.Lcd_WriteIndex(0xC2)
        self.SPI.Lcd_WriteData(0x0A) 
        self.SPI.Lcd_WriteData(0x00) 
    
        self.SPI.Lcd_WriteIndex(0xC3) 
        self.SPI.Lcd_WriteData(0x8A) 
        self.SPI.Lcd_WriteData(0x2A) 
        self.SPI.Lcd_WriteIndex(0xC4) 
        self.SPI.Lcd_WriteData(0x8A) 
        self.SPI.Lcd_WriteData(0xEE) 
        
        self.SPI.Lcd_WriteIndex(0xC5) 
        self.SPI.Lcd_WriteData(0x0E) 
        
        self.SPI.Lcd_WriteIndex(0x36)
        self.SPI.Lcd_WriteData(0xC0) 
    
        self.SPI.Lcd_WriteIndex(0xe0) 
        self.SPI.Lcd_WriteData(0x0f)
        self.SPI.Lcd_WriteData(0x1a)
        self.SPI.Lcd_WriteData(0x0f)
        self.SPI.Lcd_WriteData(0x18) 
        self.SPI.Lcd_WriteData(0x2f) 
        self.SPI.Lcd_WriteData(0x28) 
        self.SPI.Lcd_WriteData(0x20) 
        self.SPI.Lcd_WriteData(0x22) 
        self.SPI.Lcd_WriteData(0x1f) 
        self.SPI.Lcd_WriteData(0x1b) 
        self.SPI.Lcd_WriteData(0x23) 
        self.SPI.Lcd_WriteData(0x37) 
        self.SPI.Lcd_WriteData(0x00)    
        self.SPI.Lcd_WriteData(0x07) 
        self.SPI.Lcd_WriteData(0x02) 
        self.SPI.Lcd_WriteData(0x10)
    
        self.SPI.Lcd_WriteIndex(0xe1) 
        self.SPI.Lcd_WriteData(0x0f) 
        self.SPI.Lcd_WriteData(0x1b) 
        self.SPI.Lcd_WriteData(0x0f) 
        self.SPI.Lcd_WriteData(0x17)
        self.SPI.Lcd_WriteData(0x33)
        self.SPI.Lcd_WriteData(0x2c)
        self.SPI.Lcd_WriteData(0x29) 
        self.SPI.Lcd_WriteData(0x2e)
        self.SPI.Lcd_WriteData(0x30)
        self.SPI.Lcd_WriteData(0x30) 
        self.SPI.Lcd_WriteData(0x39) 
        self.SPI.Lcd_WriteData(0x3f) 
        self.SPI.Lcd_WriteData(0x00) 
        self.SPI.Lcd_WriteData(0x07) 
        self.SPI.Lcd_WriteData(0x03) 
        self.SPI.Lcd_WriteData(0x10)  
        
        self.SPI.Lcd_WriteIndex(0x2a)
        self.SPI.Lcd_WriteData(0x00)
        self.SPI.Lcd_WriteData(0x00)
        self.SPI.Lcd_WriteData(0x00)
        self.SPI.Lcd_WriteData(0x7f)
    
        self.SPI.Lcd_WriteIndex(0x2b)
        self.SPI.Lcd_WriteData(0x00)
        self.SPI.Lcd_WriteData(0x00)
        self.SPI.Lcd_WriteData(0x00)
        self.SPI.Lcd_WriteData(0x9f)
        
        self.SPI.Lcd_WriteIndex(0xF0)  
        self.SPI.Lcd_WriteData(0x01) 
        self.SPI.Lcd_WriteIndex(0xF6)
        self.SPI.Lcd_WriteData(0x00)
        
        self.SPI.Lcd_WriteIndex(0x3A)
        self.SPI.Lcd_WriteData(0x05)
        
        
        self.SPI.Lcd_WriteIndex(0x29)
        self.SPI.BL.value(1)
        self.Lcd_Clear(WHITE)
    
    '''
    函数名：LCD_Set_Region
    功能：设置lcd显示区域，在此区域写点数据自动换行
    入口参数：xy起点和终点
    返回值：无
    '''
    
    def Lcd_SetRegion(self, x_start, y_start, x_end, y_end):
        self.SPI.Lcd_WriteIndex(0x2a)
        self.SPI.Lcd_WriteData(0x00)
        self.SPI.Lcd_WriteData(x_start+2)
        self.SPI.Lcd_WriteData(0x00)
        self.SPI.Lcd_WriteData(x_end+2)
    
        self.SPI.Lcd_WriteIndex(0x2b)
        self.SPI.Lcd_WriteData(0x00)
        self.SPI.Lcd_WriteData(y_start+1)
        self.SPI.Lcd_WriteData(0x00)
        self.SPI.Lcd_WriteData(y_end+1)
        
        self.SPI.Lcd_WriteIndex(0x2c)
    
    
    '''
    函数名：LCD_Set_XY
    功能：设置lcd显示起始点
    入口参数：xy坐标
    返回值：无
    '''
    def Lcd_SetXY(self, x,y):
        self.Lcd_SetRegion(x,y,x,y)
    
    '''
    函数名：LCD_DrawPoint
    功能：画一个点
    入口参数：无
    返回值：无
    '''
    def Gui_DrawPoint(self, x, y, Data):
        self.Lcd_SetRegion(x,y,x+1,y+1)
        self.SPI.LCD_WriteData_16Bit(Data)
    
    '''
     函数功能：读TFT某一点的颜色                          
     出口参数：color  点颜色值                                 
    '''
    def Lcd_ReadPoint(self, x, y):
        Data = 0
        self.Lcd_SetXY(x,y)
        self.SPI.Lcd_WriteData(Data)
        return Data
    
    '''
    函数名：Lcd_Clear
    功能：全屏清屏函数
    入口参数：填充颜色COLOR
    返回值：无
    '''
    def Lcd_Clear(self, Color): 
        machine.freq(160000000)               
        self.Lcd_SetRegion(0,0,X_MAX_PIXEL-1,Y_MAX_PIXEL-1)
        self.SPI.Lcd_WriteIndex(0x2C)
        for i in range(X_MAX_PIXEL):
            for j in range(Y_MAX_PIXEL):
                self.SPI.LCD_WriteData_16Bit(Color)
        machine.freq(80000000)  


    
        
